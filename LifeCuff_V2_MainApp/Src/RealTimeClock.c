#include "RealTimeClock.h"
#include "DebugUart.h"
#include "main.h"

extern RTC_HandleTypeDef hrtc;

char Rtctime[10];
char Rtcdate[10];
char RtcDateTime[40];

RTC_TimeTypeDef sTime;
RTC_DateTypeDef sDate;

/*
RTC_WEEKDAY_MONDAY             ((uint8_t)0x01U)
RTC_WEEKDAY_TUESDAY            ((uint8_t)0x02U)
RTC_WEEKDAY_WEDNESDAY          ((uint8_t)0x03U)
RTC_WEEKDAY_THURSDAY           ((uint8_t)0x04U)
RTC_WEEKDAY_FRIDAY             ((uint8_t)0x05U)
RTC_WEEKDAY_SATURDAY           ((uint8_t)0x06U)
RTC_WEEKDAY_SUNDAY             ((uint8_t)0x07U)
*/

void Rtc_Init(void)
{
  
  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */
  /** Initialize RTC Only
  */
 hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_12;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }


}


void Rtc_SetTime (uint8_t hours, uint8_t minutes, uint8_t seconds, 
                  uint8_t weekday, uint8_t month, uint8_t day,
                  uint8_t year
                  )
{
  
  /**Initialize RTC and set the Time and Date
  */
  //sTime.Hours = 0x10;
  sTime.Hours = hours;
  //sTime.Minutes = 0x20;
  sTime.Minutes = minutes;
  //sTime.Seconds = 0x30;
  sTime.Seconds = seconds;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }
  /* USER CODE BEGIN RTC_Init 3 */
  
  /* USER CODE END RTC_Init 3 */
  
  //sDate.WeekDay = RTC_WEEKDAY_TUESDAY;
  sDate.WeekDay = weekday;
  //sDate.Month = RTC_MONTH_AUGUST;
  sDate.Month = month;
  //sDate.Date = 0x12;
  sDate.Date = day;
  //sDate.Year = 0x0;
  sDate.Year = year;
    
  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    //_Error_Handler(__FILE__, __LINE__);
  }
  /* USER CODE BEGIN RTC_Init 4 */
  
  HAL_RTCEx_BKUPWrite(&hrtc, RTC_BKP_DR1, 0x32F2);  // backup register
  
  /* USER CODE END RTC_Init 4 */
}

void Rtc_GetTime(void)
{
  RTC_DateTypeDef gDate;
  RTC_TimeTypeDef gTime;

  /* Get the RTC current Time */
  HAL_RTC_GetTime(&hrtc, &gTime, RTC_FORMAT_BIN);
  /* Get the RTC current Date */
  HAL_RTC_GetDate(&hrtc, &gDate, RTC_FORMAT_BCD);

  /* Display time Format: hh:mm:ss */
  sprintf((char*)Rtctime,"%02d:%02d:%02d",gTime.Hours, gTime.Minutes, gTime.Seconds);
  serialPutStr(Rtctime);
  /* Display date Format: mm-dd-yy */
  sprintf((char*)Rtcdate,"%02d-%02d-%2d",gDate.Date, gDate.Month, 2000 + gDate.Year);  
  serialPutStr(Rtcdate);
  
}