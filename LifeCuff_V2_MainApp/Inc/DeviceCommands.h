#ifndef __DEVICECOMMANDS_H__
#define __DEVICECOMMANDS_H__
#include <string.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "PwmDriver.h"

enum
{
	CMD1_DATA=0,
	CMD2_DATA,
	PAY_LOADBYTE1,
	PAY_LOADBYTE2,
	PAY_LOADBYTE3,
	PAY_LOADBYTE4,
	CRC_BYTE1,
	CRC_BYTE2,
	CRC_BYTE3,
	CRC_BYTE4

};


void commandInvalid(char *Arg);
void commandReadDeviceId(char *data);
void commandGetTime(char *data);
void commandSetTime(char *data);
void CommandInvalidCmd(char *Arg);

#endif