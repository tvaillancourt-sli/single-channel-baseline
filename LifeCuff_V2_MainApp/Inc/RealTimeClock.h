#ifndef _RTC_DRIVER_H
#define _RTC_DRIVER_H
#include <stdint.h>
#include "main.h"

void Rtc_SetTime (uint8_t hours, uint8_t minutes, uint8_t seconds, 
                  uint8_t weekday, uint8_t month, uint8_t day,
                  uint8_t year
                  );
void Rtc_GetTime(void);

void Rtc_Init(void);

#endif